---
layout: front-page
unit-code: EEXXXXX
unit-name: Module Name
unit-description: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 

primary-button:
  title: AutoNOTE Features
  markdown-file: features.md
  custom-url:
secondary-button:
  title: Moodle
  markdown-file:
  custom-url: https://moodle.bath.ac.uk

navigation:
  - features
use_custom_menu: true
root: "./"
---

## Content begins

Here is where you can put any regular content. 